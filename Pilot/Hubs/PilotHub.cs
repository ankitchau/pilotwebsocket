﻿using Microsoft.AspNetCore.SignalR;
using PilotWebsocket.RethinkDatabase;
using PilotWebsocket.RethinkDatabase.Model;
using System;

namespace PilotWebsocket.Hubs
{
    public class PilotHub : Hub
    {
        public void Send(string name, string message)
        {
            // Call the broadcastMessage method to update clients.
            Clients.All.SendAsync("broadcastMessage", name, message);
        }

        public void CreateBooking(BookingModel bookingModel)
        {
            try
            {
                Booking booking = new Booking();
                BookingModel response = new BookingModel();
                response= booking.CreateBooking(bookingModel);
                Clients.All.SendAsync("bookingCreated", response);

            }
            catch(Exception ex)
            {

            }
        }
    }
}