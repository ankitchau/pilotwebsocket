﻿using PilotWebsocket.RethinkDatabase.Model;
using RethinkDb.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PilotWebsocket.RethinkDatabase
{
    public class Booking
    {
        public static RethinkDB R = RethinkDB.R;
        public RethinkDb.Driver.Net.Connection Connect()
        {
            var c = R.Connection()
                   .Hostname("13.232.123.247")
                   .Port(RethinkDBConstants.DefaultPort)
                   .Db("Pilot_dev")
                   .Timeout(60)
                   .Connect();
            return c;
        }
        public BookingModel CreateBooking(BookingModel bookingModel)
        {
            try
            {
                var connection = Connect();
                R.Db("Pilot_dev").Table("ride_booking").Insert(bookingModel).Run(connection);
                return bookingModel;
            }
            catch(Exception ex)
            {
                return null;
            }

        }

    }
}
