﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PilotWebsocket.RethinkDatabase.Model
{
    public class BookingModel
    {
        public string booking_id { get; set; }
        public DateTime booking_date { get; set; }
        public string booking_device { get; set; }
        public string passenger_id { get; set; }
        public string pilot_id { get; set; }
        public string dropto_location { get; set; }
        public string pickup_location { get; set; }
        public string actual_pickup_location { get; set; }
        public string actual_drop_location { get; set; }
        public string estimated_distance { get; set; }
        public string estimated_cost { get; set; }
        public string estimated_time { get; set; }
        public string map_route { get; set; }
        public string preferred_gender { get; set; }
        public string preferrred_payment_method { get; set; }
        public string feedback_stars { get; set; }
        public string remark { get; set; }
        public string accepted_location { get; set; }
        public string accepted_time { get; set; }
        public string ride_status { get; set; }
        public string booking_code { get; set; }
        public string booking_reserved { get; set; }
    }
}
