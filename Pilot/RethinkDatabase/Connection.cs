﻿using RethinkDb.Driver;
using Microsoft.AspNet.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PilotWebsocket.RethinkDatabase
{
    public class Connection
    {
        public static RethinkDB R = RethinkDB.R;
        public void Connect()
        {
            var c = R.Connection()
                   .Hostname("13.232.123.247")
                   .Port(RethinkDBConstants.DefaultPort)
                   .Db("Test")
                   .Timeout(60)
                   .Connect();
        }
    }
}
